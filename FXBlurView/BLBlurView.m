//
//  BLBlurView.m
//  AppDay
//
//  Created by David on 25/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import "BLBlurView.h"

@implementation BLBlurView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.barStyle=UIBarStyleBlack;
        self.translucent=YES;
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.barStyle=UIBarStyleBlack;
        self.translucent=YES;
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
