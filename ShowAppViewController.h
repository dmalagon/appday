//
//  ShowAppViewController.h
//  AppDay
//
//  Created by David on 22/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "AppInfo.h"
#import "FXBlurView.h"


@interface ShowAppViewController : UIViewController <iCarouselDataSource, iCarouselDelegate>

@property (nonatomic, strong) AppInfo * currentAppInfo;
@property (nonatomic, weak) IBOutlet iCarousel *carousel;
@property (nonatomic, weak) IBOutlet FXBlurView *blurView;
@property (nonatomic) NSInteger index;
@property (weak, nonatomic) IBOutlet UIImageView *appIconDetail;
@property (nonatomic, weak) IBOutlet UILabel *appTextTitleDetail;
@property (nonatomic, weak) IBOutlet UILabel *appDeveloperDetail;
@property (nonatomic, weak) IBOutlet UITextView *appDescripcionDetail;
@property (nonatomic, weak) IBOutlet UILabel *appPuntuacionDetail;
@property (weak, nonatomic) IBOutlet UIButton *fbButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
@property (weak, nonatomic) IBOutlet UIButton *leadButton;
@property (nonatomic,strong) UIView *coverView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIView *scoremeterHolderView;
@property (weak, nonatomic) IBOutlet UILabel *appPrecioAnterior;


- (IBAction)goToAppStoreUrl:(id)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil index:(NSInteger)index;
- (IBAction)tappedDetail;
- (IBAction)tappedCaroussel;
- (IBAction)postToTwitter:(id)sender;
- (IBAction)postToFacebook:(id)sender;
- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;
@end
