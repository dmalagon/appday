//
//  SplashViewController.m
//  VoilaApp
//
//  Created by David on 14/10/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import "SplashViewController.h"
#import "CurrentApps.h"


#define kwebServicesUrl @"http://www.moneyback.es/ws/clc.asmx/appday_apps"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"splash_logo_v3.png"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("loader", NULL);
    dispatch_async(downloadQueue, ^{
        
        [[CurrentApps defaultColeccion] loadAppsFromURL:kwebServicesUrl];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    });
    
    
}

@end
