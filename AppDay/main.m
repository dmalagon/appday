//
//  main.m
//  AppDay
//
//  Created by David on 19/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
