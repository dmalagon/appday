//
//  AppInfo.h
//  AppDay
//
//  Created by David on 20/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppInfo : NSObject

@property (nonatomic) int idApp;
@property (nonatomic,strong) NSString *nombreApp;
@property (nonatomic,strong) NSString *developerApp;
@property (nonatomic,strong) NSString *resumenApp;
@property (nonatomic,strong) NSString *descripcionApp;
@property (nonatomic,strong) UIImage *appIcon;
@property (nonatomic,strong) NSMutableArray *galeriaCreatividades; //Array de imágenes de las creatividades
@property (nonatomic,strong) NSString *urlAppStore;
@property (nonatomic,strong) NSString *urlFacebookLike;
@property (nonatomic) float precioNormal;
@property (nonatomic) float precioOferta;
@property (nonatomic,strong) NSDate *fechaOferta;
@property (nonatomic) int puntuacionEditor;


-(id)initWithIdApp:(int)idApp;


@end
