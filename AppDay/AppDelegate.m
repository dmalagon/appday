//
//  AppDelegate.m
//  AppDay
//
//  Created by David on 19/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import "AppDelegate.h"

#import "ShowAppViewController.h"
#import "ReverseNavigationControllerViewController.h"
#import "SplashViewController.h"
#import <MobileAppTracker/MobileAppTracker.h>

#define MAT_ADVERTISER_ID @"10312"
#define MAT_CONVERSION_KEY @"608c4b86e4c6dde3b1f7942c60582fe2"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] ;
    
    // Setup transitionController
    ShowAppViewController *viewController = [[ShowAppViewController alloc] initWithNibName:@"ShowAppViewController" bundle:nil index:0];
    //ADTransitionController * transitionController = [[ADTransitionController alloc] initWithRootViewController:viewController];
    ReverseNavigationControllerViewController * transitionController = [[ReverseNavigationControllerViewController alloc] initWithRootViewController:viewController];
    
    self.window.rootViewController = transitionController;
    

    
    transitionController.navigationController.navigationBarHidden=YES;      
    
    // Setup appearance
    
    //[[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"app1.png"] stretchableImageWithLeftCapWidth:1.0f topCapHeight:1.0f] forBarMetrics:UIBarMetricsDefault];
    //[[UINavigationBar appearance] setBarStyle:UIBarStyleBlackTranslucent];
    //[[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.906 green:0.784 blue:0 alpha:1]];
     
//    NSDictionary * navigationBarTextAttributes = @{UITextAttributeTextColor : [UIColor whiteColor],
//                                                   UITextAttributeTextShadowColor : [UIColor blackColor],
//                                                   UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetMake(-1.0f, 0)]};
//    [[UINavigationBar appearance] setTitleTextAttributes:navigationBarTextAttributes];
//    
//    [[UIBarButtonItem appearance] setBackgroundImage:[[UIImage imageNamed:@"ALDoneButtonOff"] stretchableImageWithLeftCapWidth:5.0f topCapHeight:5.0f] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    [[UIBarButtonItem appearance] setBackgroundImage:[[UIImage imageNamed:@"ALDoneButtonOn"] stretchableImageWithLeftCapWidth:5.0f topCapHeight:5.0f] forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
//    NSDictionary * barButtonItemTextAttributes = @{UITextAttributeTextColor : [UIColor whiteColor],
//                                                   UITextAttributeFont : [UIFont systemFontOfSize:14.0]};
//    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonItemTextAttributes forState:UIControlStateNormal];
    
    // Remove status bar
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackTranslucent;
    
    self.window.backgroundColor = [UIColor blackColor];
    
    
    //Splashh ON
//    SplashViewController *splashScreenViewController = [[SplashViewController alloc] initWithNibName:nil bundle:nil];
//    splashScreenViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self.window.rootViewController presentViewController:splashScreenViewController animated:NO completion:nil];
    
    
    
    [[MobileAppTracker sharedManager] startTrackerWithMATAdvertiserId:MAT_ADVERTISER_ID MATConversionKey:MAT_CONVERSION_KEY];
    [[MobileAppTracker sharedManager] trackInstall];
    //Splash OFF
    [self.window makeKeyAndVisible];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
