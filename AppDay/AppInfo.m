//
//  AppInfo.m
//  AppDay
//
//  Created by David on 20/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import "AppInfo.h"

@interface AppInfo ()

@end

@implementation AppInfo

-(id)init{

    return [self initWithIdApp:0];
}

-(id)initWithIdApp:(int)idApp{
    
    self=[super init];
    
    if (self){
        self.idApp=idApp;
        
        self.nombreApp=@"";
        self.developerApp=@"";
        self.resumenApp=@"";
        self.descripcionApp=@"";
        self.appIcon=nil;
        self.galeriaCreatividades= [[NSMutableArray alloc] init];
        self.urlAppStore=@"";
        self.urlFacebookLike=@"";
        self.precioNormal=0;
        self.precioOferta=0;
        self.fechaOferta=[[NSDate date] init];
        self.puntuacionEditor=0;
    }
    
    return self;
}


@end
