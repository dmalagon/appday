//
//  CurrentApps.h
//  AppDay
//
//  Created by David on 20/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//
#import "AppInfo.h"
#import <Foundation/Foundation.h>


//@interface CurrentApps : NSObject <NSURLConnectionDelegate>
@interface CurrentApps : NSObject

+ (CurrentApps *)defaultColeccion;

- (NSArray *)allApps;
- (void)loadAppsFromURL:(NSString *) url;
- (void)addApp:(AppInfo*)newapp;
@end
