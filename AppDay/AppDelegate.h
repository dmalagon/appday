//
//  AppDelegate.h
//  AppDay
//
//  Created by David on 19/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
