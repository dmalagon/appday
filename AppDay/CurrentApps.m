//
//  CurrentApps.m
//  AppDay
//
//  Created by David on 20/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import "CurrentApps.h"
#import "AppInfo.h"



static CurrentApps *defaultColeccion = nil;

@implementation CurrentApps

{
    NSMutableArray * allApps;

}

+ (CurrentApps *)defaultColeccion
{
    if (!defaultColeccion) {
        // Crear singleton
        defaultColeccion = [[super allocWithZone:NULL] init];
    }
    return defaultColeccion;
}

// Prevenir la creacion de instancias adicionales
+ (id)allocWithZone:(NSZone *)zone
{
    return [self defaultColeccion];
}

- (id)init
{
    
    if (defaultColeccion) {
        
        
        return defaultColeccion;
    }
    
    self = [super init];
    return self;
}

-(NSArray *)allApps{
    
    if (!allApps) {
        allApps = [[NSMutableArray alloc] init];
    }
    
    return allApps;
}

# pragma mark CurrentApps Methods
- (void)addApp:(AppInfo*)newapp{
    
    if(!allApps) {
        allApps = [[NSMutableArray alloc] init];
    }
    [allApps addObject:newapp];
    
}

- (void)loadAppsFromURL:(NSString *) url
{
    NSString *paramsSolicitud=[NSString stringWithFormat:@"{'pw':'%@','iphone_id':'%@'}",@"LiarParda",[[[UIDevice currentDevice] identifierForVendor] UUIDString]];   

    
    NSDictionary *jsonResult=[self invocarWebService:url params:paramsSolicitud];
    
    
    NSArray *lista_datos=[jsonResult objectForKey:@"d"]; //Aquí solo hay un elemento en el array, que contendrá los datos.
    
    //NSDictionary *datos_item=[lista_datos objectAtIndex:0];//A este nivel están los datos. //
    for (NSDictionary *datos_item in lista_datos) {
        AppInfo * newAppInfo=[[AppInfo alloc] initWithIdApp:(int)datos_item[@"id"]];
        
        newAppInfo.nombreApp=datos_item[@"nombre_app"];
        newAppInfo.developerApp=datos_item[@"developer_app"];
        newAppInfo.resumenApp=datos_item[@"resumen_app"];
        newAppInfo.descripcionApp=datos_item[@"descripcion_app"];
        
        //Cargar app icon
        NSURL *url_icono = [NSURL URLWithString:datos_item[@"url_icono_app"]];
        NSData *iconImageData = [NSData dataWithContentsOfURL:url_icono];
        newAppInfo.appIcon=[[UIImage alloc] initWithData:iconImageData];
        
        //Cargar creatividades
        NSArray *lista_creatividades = [datos_item objectForKey:@"creatividades"];
        
        if (lista_creatividades!=(id)[NSNull null]) {
            //Recogemos todas las creatividades de la url y las cargamos en el objeto
            for (NSDictionary * creatividad in lista_creatividades) {
                
                //Obviamos el orden por el momento, las metemos por su orden de origen
                NSURL *url_creatividad = [NSURL URLWithString:creatividad[@"creatividad_app"]];
                NSData *creatividadImageData = [NSData dataWithContentsOfURL:url_creatividad];
                
                [newAppInfo.galeriaCreatividades addObject:[[UIImage alloc] initWithData:creatividadImageData]];
            }
        }else{
            //NO hay creatividades
            newAppInfo.galeriaCreatividades=nil;
        }
        
        newAppInfo.urlAppStore=datos_item[@"url_app_store"];
        newAppInfo.urlFacebookLike=datos_item[@"url_facebook"];
        newAppInfo.precioNormal=[datos_item[@"precio_normal"] floatValue];
        newAppInfo.precioOferta=[datos_item[@"precio_oferta"] floatValue];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        newAppInfo.fechaOferta= [dateFormat dateFromString:datos_item[@"fecha_oferta_app"]];
        
        newAppInfo.puntuacionEditor=[datos_item[@"puntuacion"] integerValue];
        
        [self addApp:newAppInfo];
    }

}


# pragma mark Web Services Methods

-(NSDictionary*)invocarWebService:(NSString*)urlString params:(NSString*) paramsString{
    
    //Version de la aplicación
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/json,text/javascript" forHTTPHeaderField:@"Accept"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json;charSet=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    //PAra servicio TT:NSString *param1String=[NSString stringWithFormat:@"{'BN':'%@','vs':'%@','pw':'LiarParda'}",@"",version];
    //Para servicio HT_reg: NSString *param1String=[NSString stringWithFormat:@"{'BN':'%@','vs':'%@','Id_reg':'%d','Error_contenido':'%@'}",@"",version,idRegistro,@""];
    NSString *param1String=paramsString;
    
    NSData *requestData = [NSData dataWithBytes:[param1String UTF8String] length:[param1String length]];
    [request setHTTPBody: requestData];
    
    NSError *errorReturned = nil;
    NSURLResponse *theResponse =[[NSURLResponse alloc]init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&theResponse error:&errorReturned];
    
    
    if (errorReturned)
    {
        NSLog(@"Error al realizar la conexión");
        return nil;
    }
    else
    {
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        NSLog(@"json: %@",json);
        return json;
    }
}


//#pragma mark NSURLConnection Delegate Methods
//
//- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
//    // A response has been received, this is where we initialize the instance var you created
//    // so that we can append data to it in the didReceiveData method
//    // Furthermore, this method is called each time there is a redirect so reinitializing it
//    // also serves to clear it
//    _responseData = [[NSMutableData alloc] init];
//}
//
//- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
//    // Append the new data to the instance variable you declared
//    [_responseData appendData:data];
//}
//
//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
//                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
//    // Return nil to indicate not necessary to store a cached response for this connection
//    return nil;
//}
//
//- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
//    // The request is complete and data has been received
//    // You can parse the stuff in your instance variable now
//    
//}
//
//- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
//    // The request has failed for some reason!
//    // Check the error var
//}
@end
