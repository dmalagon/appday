//
//  ShowAppViewController.m
//  AppDay
//
//  Created by David on 22/09/13.
//  Copyright (c) 2013 dastudio. All rights reserved.
//

#import "ShowAppViewController.h"
#import "AppInfo.h"
#import "CurrentApps.h"
#import "AppDelegate.h"
#import "UIView+MTAnimation.h"
#import <Social/Social.h>
#import "SplashViewController.h"


#define kwebServicesUrl @"http://www.moneyback.es/ws/clc.asmx/appday_apps"



@interface ShowAppViewController ()

@property (nonatomic, strong) NSMutableArray *items;

@property (nonatomic) CGFloat deviceHeight;
@property (nonatomic) CGPoint logoCenterTop;
@property (nonatomic) CGPoint logoCenterOffScreen;
@property (nonatomic) CGFloat detailTopY;
@property (nonatomic) CGFloat detailBottomY;

@end

@implementation ShowAppViewController

- (IBAction)goToAppStoreUrl:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.currentAppInfo.urlAppStore]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil index:(NSInteger)index
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.index=index;//Index es el numero de app en el array
        
        if (self.index==0 && [[CurrentApps defaultColeccion] allApps].count==0) {
            
            [[CurrentApps defaultColeccion] loadAppsFromURL:kwebServicesUrl];
        }
        
        //***** Cargamos la app adecuada en el viewcontroller
        //[[CurrentApps defaultColeccion] loadAppsFromURL:kwebServicesUrl];
        self.currentAppInfo= [[CurrentApps defaultColeccion] allApps][index];
        
        AppDelegate  *app =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.deviceHeight = app.window.frame.size.height;
        
        
        //***** Segun la versión de ios, adaptamos algunos elementos del interface
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
            // Load resources for iOS 6.1 or earlier
            
            //Posiciones para ios6, que mide con la barra de estado
            self.logoCenterTop=CGPointMake(160,83);
            self.logoCenterOffScreen=CGPointMake(160,-100);
            self.detailTopY=123;
            self.detailBottomY=self.deviceHeight-140;
            
            [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"fondo_nav2_ios6.png"]  forBarMetrics:UIBarMetricsDefault];
            //self.navigationController.navigationBar.clipsToBounds=YES;
            [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"fondo_detail_ios6.png"] forToolbarPosition:UIBarPositionBottom barMetrics:UIBarMetricsDefault];
            [UIApplication sharedApplication].statusBarStyle =UIStatusBarStyleDefault;
            
            
        } else {
            // Load resources for iOS 7 or later
            self.logoCenterTop=CGPointMake(160,103);
            self.logoCenterOffScreen=CGPointMake(160,-100);
            self.detailTopY=143;
            self.detailBottomY=self.deviceHeight-120;
            
            //[[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"app1.png"] stretchableImageWithLeftCapWidth:1.0f topCapHeight:1.0f] forBarMetrics:UIBarMetricsDefault];
            [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackTranslucent];
            [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.906 green:0.784 blue:0 alpha:1]];
            //[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.13 green:0.28 blue:0.37 alpha:1.0]];//Color Azul para el fondo
        }
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"app1.png"] stretchableImageWithLeftCapWidth:1.0f topCapHeight:1.0f] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBarStyle: UIBarStyleBlackTranslucent];
    //self.navigationController.navigationBar.barTintColor=[[UIColor alloc] initWithRed:0.24 green:0.24 blue:0.24 alpha:0.6];
//    [self.navigationController.navigationBar setTranslucent: YES];
//    [self.navigationController.navigationBar setOpaque:NO];
    
    
    //***** Configuracion IMAGENES FONDO
    _carousel.backgroundColor=[[UIColor alloc] initWithRed:0.15 green:0.15 blue:0.15 alpha:1];
    _carousel.frame=  [[UIScreen mainScreen] bounds];
    _carousel.type = iCarouselTypeCylinder;
    _carousel.pagingEnabled = YES;
    
    //self.blurView.dynamic = YES;
    //self.blurView.tintColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    //[self.blurView.layer displayIfNeeded]; //force immediate redraw
    //self.blurView.contentMode = UIViewContentModeBottom;
    
    //self.blurView.iterations=1;


    //***** Comprobamos si hay mas de una app cargada. Si es así, cargamos el array de viewcontrollers del NAvigationController para ordenar inversamente las apps.
    if (self.index==0) {
        if ([[CurrentApps defaultColeccion] allApps].count>1 && self.navigationController.viewControllers.count==1) {
            NSMutableArray *vcs =  [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            
            for (int i=1; i<[[CurrentApps defaultColeccion] allApps].count; i++) {
                
                ShowAppViewController *viewController = [[ShowAppViewController alloc] initWithNibName:@"ShowAppViewController" bundle:nil index:i];
                //[vcs insertObject:viewController atIndex:[vcs count]-1];
                viewController.navigationItem.title=@"ANTERIOR";
                
                
                [vcs insertObject:viewController atIndex:0];

            }
            [self.navigationController setViewControllers:vcs animated:NO];
            //[self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"VOLVER" style: UIBarButtonItemStyleBordered target:self action:@selector(NextApp)];
        //backButton.image=[UIImage imageNamed:@"right_arrow.png"];
        //[backButton setBackgroundImage:[[UIImage imageNamed:@"right_arrow.png" ] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
        self.navigationItem.rightBarButtonItem = backButton;
    }
    
    //***** Configuración TITULO
    if (self.index==0) {
        self.title = @"HOY";//[NSString stringWithFormat:@"Index: %d", self.index];

        
    }else if(self.index==1){
        self.title = @"AYER";//[NSString stringWithFormat:@"Index: %d", self.index];
        
    }else if (self.index>1){
        self.title = [NSString stringWithFormat:@"Dia: %d", (int)-self.index];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        self.title =[dateFormat stringFromDate:self.currentAppInfo.fechaOferta];
    }
    
    //self.title=[NSString stringWithFormat:@"%d", self.navigationController.viewControllers.count ];
    //self.title=[NSString stringWithFormat:@"Index: %d", self.index];
    
    //****Cargar scoremeter
    //
    for (int i=0; i<10; i++) {
        int offsetY=20;
        UIImageView* score = [[UIImageView alloc] initWithFrame:CGRectMake(offsetY*i, 0, 15, 15)];
        NSString * currentImageName;
        if (self.currentAppInfo.puntuacionEditor >=(i+1)) {
            currentImageName=[NSString stringWithFormat:@"%d.png",i+1];
        }else{
            currentImageName=@"no_score_gray.png";
        }
        
        score.image=[UIImage imageNamed:currentImageName];
        [self.scoremeterHolderView addSubview:score];
    }
}

-(void)NextApp{
    //Acción custom para hacer un push del viewcontroller
    ShowAppViewController *viewController = [[ShowAppViewController alloc] initWithNibName:@"ShowAppViewController" bundle:nil index:self.index-1];
    
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark -
#pragma mark SetUp Views

-(void) viewWillAppear:(BOOL)animated{
    
    UIImage* scaledImage=[UIImage imageWithCGImage:[self.currentAppInfo.appIcon CGImage] scale:2.0 orientation:UIImageOrientationUp];
    [self.appIconDetail setImage:scaledImage];
    self.appTextTitleDetail.text=self.currentAppInfo.nombreApp;
    self.appDeveloperDetail.text=self.currentAppInfo.developerApp;
    
    NSString * precioActual=nil;
    if (self.currentAppInfo.precioOferta==0) {
        precioActual=@"Ahora: Gratis";
    }else{
        precioActual=[NSString stringWithFormat:@"Ahora: %.2f €",self.currentAppInfo.precioOferta];
    
    }
    
    [self.leadButton setTitle:precioActual forState:UIControlStateNormal];
   
    if (self.currentAppInfo.precioNormal==0) {
        self.appPrecioAnterior.text=@"";
        //self.leadButton =[NSString stringWithFormat:@"Ahora: %@",precioActual];
    }
    else{
        self.appPrecioAnterior.text=[NSString stringWithFormat:@"Antes: %.2f €",self.currentAppInfo.precioNormal];
    }
    
    
    [self.appDescripcionDetail setText:self.currentAppInfo.descripcionApp];
    [self.appDescripcionDetail setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:15]];
    [self.appDescripcionDetail setTextColor:[UIColor whiteColor]];
    
    self.appPuntuacionDetail.text=[NSString stringWithFormat:@"%d",self.currentAppInfo.puntuacionEditor];
    
    [self.appIconDetail.layer setCornerRadius:3.0f];
    self.appIconDetail.layer.masksToBounds=YES;
    [self.leadButton.layer setCornerRadius:3.0f];
    self.leadButton.layer.masksToBounds=YES;
    [self.fbButton.layer setCornerRadius:3.0f];
    self.fbButton.layer.masksToBounds=YES;
    [self.twitterButton.layer setCornerRadius:3.0f];
    self.twitterButton.layer.masksToBounds=YES;
    
    self.logoImageView.image=[UIImage imageNamed:@"logoVoila_cuadros3.png"];
    
    

    
    
    
//    [self.logoImageView.layer setShadowColor:[UIColor blackColor].CGColor];
//    [self.labelAppDay.layer setShadowRadius:3.0];
//    [self.labelAppDay.layer setShadowOpacity:0.9];
//    [self.labelAppDay.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
//    self.labelAppDay.layer.masksToBounds=NO;
//    self.labelAppDay.clipsToBounds=NO;
    
}
-(void)viewDidAppear:(BOOL)animated{
    //if (self.index==0) {

//        [UIView mt_animateViews:@[self.blurView] duration:0.4 timingFunction:kMTEaseOutBack animations:^{
//            //BOOL close = self.blurView.frame.size.height > 300;
//            //self.blurView.frame = CGRectMake(0, close? self.deviceHeight-120: 143, 320, open?130: 425);//
//            BOOL close =self.blurView.frame.origin.y<self.deviceHeight-120;
//            
//            CGRect updatedFrame= self.blurView.frame;
//            updatedFrame.origin.x=0;
//            
//            if (close) {
//                updatedFrame.origin.y=self.deviceHeight-120;
//                
//            }else{
//                updatedFrame.origin.y=103;
//            }
//            
//            self.blurView.frame=updatedFrame;
//        } completion:^{
//            NSLog(@"completed");
//        }];
//        
//        
//        //Sube logo
//        [UIView animateWithDuration:0.3 animations:^{
//            BOOL close =self.blurView.frame.origin.y<self.deviceHeight-120;
//            
//            if (close){
//                //updatedFrame.origin.y=47;
//                self.logoImageView.center=CGPointMake(160,103);
//                self.logoImageView.alpha=1;
//            }else{
//                //updatedFrame.origin.y=-50;
//                self.logoImageView.center=CGPointMake(160,-100);
//                self.logoImageView.alpha=0;
//            }
//            //updatedFrame.origin.x=44;
//            
//        }];
    

        [self animateDetailPanel];
    //}

}


-(void)viewDidLayoutSubviews{
    //Reubicación inicial por codigo
    

    //Set up controls for small screen
    
//    if (self.deviceHeight<568) {
//        self.appDescripcionDetail.frame=CGRectMake(self.appDescripcionDetail.frame.origin.x,self.appDescripcionDetail.frame.origin.y,self.appDescripcionDetail.frame.size.width,130);
//        self.twitterButton.frame= CGRectMake(self.twitterButton.frame.origin.x, self.twitterButton.frame.origin.y-50,self.twitterButton.frame.size.width,self.twitterButton.frame.size.height);//self.twitterButton.frame.origin.y-50
//    }
    
    self.blurView.frame = CGRectMake(0, self.detailBottomY, 320, 568); //243 solo se ve la cabecera
    
    self.logoImageView.center=self.logoCenterOffScreen;
    self.logoImageView.alpha=0;
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.currentAppInfo=nil;
    self.carousel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    
    return self.currentAppInfo.galeriaCreatividades.count;
    //return 4;

}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIView alloc] initWithFrame:[_carousel bounds]];
        
        //UIImage* scaledImage=[UIImage imageWithCGImage:[self.currentAppInfo.galeriaCreatividades[index] CGImage] scale:UIScreen.mainScreen.scale orientation:UIImageOrientationUp];
        UIImage* scaledImage=[UIImage imageWithCGImage:[self.currentAppInfo.galeriaCreatividades[index] CGImage] scale:2.0 orientation:UIImageOrientationUp];
        UIImageView* iView = [[UIImageView alloc] initWithImage:scaledImage];
        //UIImageView* iView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"fondo%d.jpg",index]]];
        view =iView;

    }
    
    return view;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    
    _carousel.delegate = nil;
    _carousel.dataSource = nil;
    
}
- (BOOL)carousel:(iCarousel *)carousel shouldSelectItemAtIndex:(NSInteger)index{
    
    //[self tappedCaroussel];
    
    return NO;
}
#pragma mark Gestures
- (IBAction)tappedCaroussel
{
    //    [UIView animateWithDuration:0.3 animations:^{
    //
    //        BOOL open = self.blurView.frame.size.height > 300;
    //        self.blurView.frame = CGRectMake(0, open? self.deviceHeight-120: 143, 320, open?130: 425);//
    //    }];
    
    //Toggle Oculta NAvigationVar
    [self.navigationController setNavigationBarHidden:!self.navigationController.navigationBarHidden animated:YES];
    //Toggle Oculta logo
    [UIView animateWithDuration:0.3 animations:^{
        
        if (self.blurView.alpha==0) {
            self.blurView.alpha=1;
            //self.labelAppDay.alpha=1;
        }else{
            self.blurView.alpha=0;
        }
        
        self.logoImageView.alpha=0;
        
        BOOL open;
        if (self.navigationController.navigationBarHidden) {
            open=YES;
            //[UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleDefault;
        }else{
            open=NO;
            //[UIApplication sharedApplication].statusBarStyle=UIStatusBarStyleLightContent;
        }
        self.blurView.frame = CGRectMake(0, open? self.deviceHeight: self.detailBottomY, 320, open?0: 568);
    }];
    
}




- (IBAction)tappedDetail
{
//    [UIView animateWithDuration:0.3 animations:^{
//        
//        //BOOL close = self.blurView.frame.size.height > 300;
//        //self.blurView.frame = CGRectMake(0, close? self.deviceHeight-120: 143, 320, open?130: 425);//
//        BOOL close =self.blurView.frame.origin.y<self.deviceHeight-120;
//        
//        CGRect updatedFrame= self.blurView.frame;
//        updatedFrame.origin.x=0;
//        
//        if (close) {
//            updatedFrame.origin.y=self.deviceHeight-120;
//            
//        }else{
//            updatedFrame.origin.y=143;
//        }
//        
//        self.blurView.frame=updatedFrame;
//        
//    }];
    
    [self animateDetailPanel];
    
//    [self.navigationController setNavigationBarHidden:!self.navigationController.navigationBarHidden animated:YES];
//
//        [UIView animateWithDuration:0.3 animations:^{
//
//            if (self.blurView.alpha==0) {
//                self.blurView.alpha=1;
//            }else{
//                self.blurView.alpha=0;
//            }
////            BOOL open = self.blurView.frame.size.height > 300;
////            self.blurView.frame = CGRectMake(0, open? self.deviceHeight: 143, 320, open?0: 425);
//        }];
    
}



- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    
    CGPoint translation = [recognizer translationInView:self.view];
    
    //Limite superior flexible
    if (recognizer.view.frame.origin.y<self.detailTopY) {
        CGFloat distance=(self.detailTopY -recognizer.view.frame.origin.y)*0.2;
        if (distance ==0) {
            recognizer.view.center = CGPointMake(recognizer.view.center.x,
                                                 recognizer.view.center.y );
        }else{
            recognizer.view.center = CGPointMake(recognizer.view.center.x,
                                                 recognizer.view.center.y + translation.y/distance);
        }
        
        [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    }else{
        recognizer.view.center = CGPointMake(recognizer.view.center.x,
                                             recognizer.view.center.y + translation.y);
        
        
        [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    
    }

    //Levantamos el dedo!
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        //Si sueramos el limite SUPERIOR
         if (recognizer.view.frame.origin.y<self.detailTopY) {
             //BounceBack ARRIBA
             [UIView animateWithDuration:0.3 animations:^{
                 
                 CGRect updatedFrame= self.blurView.frame;
                 updatedFrame.origin.x=0;
                 updatedFrame.origin.y=self.detailTopY;
                 
                 self.blurView.frame=updatedFrame;
                 
             }];
         
         }else if (recognizer.view.frame.origin.y>self.detailBottomY) {
             //BounceBack ABAJO
             //Si superamos el limite INFERIOR
             [UIView animateWithDuration:0.3 animations:^{
                 
                 CGRect updatedFrame= self.blurView.frame;
                 updatedFrame.origin.x=0;
                 updatedFrame.origin.y=self.detailBottomY;
                 
                 self.blurView.frame=updatedFrame;
                 
             }];
             
         }else{
             //Todo lo demas, por dirección del pan
             
             CGPoint velocity = [recognizer velocityInView:self.view];
             
             if(velocity.y > 0)
             {
                 //Baja
                 
                 [UIView mt_animateViews:@[self.blurView] duration:0.3 timingFunction:kMTEaseOutBack animations:^{
                     CGRect updatedFrame= self.blurView.frame;
                     updatedFrame.origin.x=0;
                     updatedFrame.origin.y=self.detailBottomY;
                     
                     self.blurView.frame=updatedFrame;
                 } completion:^{
                     NSLog(@"completed");
                 }];
                 
                 //Baja logo
                 [UIView animateWithDuration:0.3 animations:^{

                     self.logoImageView.center=self.logoCenterOffScreen;
                     self.logoImageView.alpha=0;
                     
                 }];
             }
             else
             {
                 //Sube
                 [UIView mt_animateViews:@[self.blurView] duration:0.3 timingFunction:kMTEaseOutBack animations:^{
                     CGRect updatedFrame= self.blurView.frame;
                     updatedFrame.origin.x=0;
                     updatedFrame.origin.y=self.detailTopY;
                     
                     self.blurView.frame=updatedFrame;
                 } completion:^{
                     NSLog(@"completed");
                 }];

                 //Sube logo
                 
                 [UIView animateWithDuration:0.3 animations:^{
                     
                     self.logoImageView.center=self.logoCenterTop;
                     self.logoImageView.alpha=1;
                     
                     
                     
                 }];
             }
         }
    }
    
    
    
    //[recognizer setTranslation:CGPointMake(0, 0) inView:_myScroll];
    
}


-(void)animateDetailPanel{
    [UIView mt_animateViews:@[self.blurView] duration:0.4 timingFunction:kMTEaseOutBack animations:^{
        //BOOL close = self.blurView.frame.size.height > 300;
        //self.blurView.frame = CGRectMake(0, close? self.deviceHeight-120: 143, 320, open?130: 425);//
        BOOL close =self.blurView.frame.origin.y<self.detailBottomY;
        
        CGRect updatedFrame= self.blurView.frame;
        updatedFrame.origin.x=0;
        
        if (close) {
            updatedFrame.origin.y=self.detailBottomY;
            
        }else{
            updatedFrame.origin.y=self.detailTopY;
        }
        
        self.blurView.frame=updatedFrame;
    } completion:^{
        NSLog(@"completed");
    }];
    
    
    //Sube logo
    [UIView animateWithDuration:0.3 animations:^{
        BOOL close =self.blurView.frame.origin.y<self.detailBottomY;
        
        if (close){
            //updatedFrame.origin.y=47;
            //Baja Logo
            self.logoImageView.center=self.logoCenterTop;
            self.logoImageView.alpha=1;
        }else{
            //updatedFrame.origin.y=-50;
            //Sube logo
            self.logoImageView.center=self.logoCenterOffScreen;
            self.logoImageView.alpha=0;
        }
        
    }];
    
}

#pragma mark -
#pragma mark Social Framework methods
- (IBAction)postToTwitter:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        NSString *postContent=[NSString stringWithFormat:@"%@ está de oferta con AppDay!\n",self.currentAppInfo.nombreApp];
        [tweetSheet setInitialText:postContent];
        [tweetSheet addURL:[NSURL URLWithString:self.currentAppInfo.urlAppStore]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}


- (IBAction)postToFacebook:(id)sender {
    //if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        NSString *postContent=[NSString stringWithFormat:@"%@ está de oferta con AppDay! ",self.currentAppInfo.nombreApp];
        [controller setInitialText:postContent];
        [controller addURL:[NSURL URLWithString:self.currentAppInfo.urlAppStore]];
        [controller addImage:self.currentAppInfo.appIcon];
        [self presentViewController:controller animated:YES completion:Nil];
    //}
    
}
@end
